export interface Students {
    id?:string;
    mathematics: number;
    psychometric: number;
    pay: Boolean;
    result?:string;
}
