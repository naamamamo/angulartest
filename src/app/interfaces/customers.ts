export interface Customers {
    id:string,
    name:string;
    education:number;
    income:number;
}
