import { CustomerFormComponent } from './customer-form/customer-form.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { StudentsFormComponent } from './students-form/students-form.component';
import { StudentsComponent } from './students/students.component';
import { CustomersComponent } from './customers/customers.component';
import { PostsComponent } from './posts/posts.component';
import { SavedpostComponent } from './savedpost/savedpost.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'welcome', component: WelcomeComponent },
  { path: 'form', component: StudentsFormComponent },
  { path: 'students', component: StudentsComponent },
  { path: 'customers', component: CustomersComponent },
  { path: 'customersForm', component: CustomerFormComponent},
  { path: 'posts', component: PostsComponent },
  { path: 'saved', component: SavedpostComponent }


  






];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
