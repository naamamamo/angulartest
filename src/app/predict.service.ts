import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {

  url = ' https://hybs62w2xb.execute-api.us-east-1.amazonaws.com/naama';
  
  predict(mathematics:number, psychometric:number, pay:Boolean):Observable<any>{
    let json = {
      "data": 
        {
          "mathematics": mathematics,
          "psychometric": psychometric,
          "pay": pay
        }
    }
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }




    constructor(private http:HttpClient) { }
}