import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  email:string;

  constructor(public auth:AuthService) { }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => {
      this.email = user.email;
       }
        )
      }       
          }
