import { Students } from './../interfaces/students';
import { Component, OnInit } from '@angular/core';
import { PredictService } from '../predict.service';
import { AuthService } from '../auth.service';
import { StudentService } from '../student.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-students-form',
  templateUrl: './students-form.component.html',
  styleUrls: ['./students-form.component.css']
})
export class StudentsFormComponent implements OnInit {

  student:Students;
  mathematics: number;
  psychometric: number;
  pay;
  result:string;
  options:Object[] = [{id:1,value:'yes'},{id:2,value:'no'}]
  option:string;
  email:string;

    add(){
    this.StudentService.addStudent(this.mathematics, this.psychometric, this.pay, this.result, this.email);
    this.router.navigate(['/students']);  
   }

    cancel(){
      this.result = null;
    }

    predict(){
        this.predictService.predict(this.mathematics,this.psychometric, this.pay).subscribe(
          res => {console.log(res);
            if(res > 0.5){
              var result = 'Will leave';
              console.log(result)
            } else {
              var result = 'Will not leave';
              console.log(result)
            }
          this.result = result;
          });  
      }
    
  constructor(private predictService:PredictService, 
              public auth:AuthService, 
              private StudentService:StudentService,
              private router:Router) { }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
      user => {
      this.email = user.email;
       }
        )
      }       
          }
                