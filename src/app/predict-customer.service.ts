import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictCustomerService {

  url = 'https://qvak2u1nd3.execute-api.us-east-1.amazonaws.com/beta';
  predict(education:number, income:number):Observable<any>{
    let json = {
        "data": {
          "education": education,
          "income":income
        }
    }

    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }



    constructor(private http:HttpClient,
               ) { }
}