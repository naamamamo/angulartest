import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-savedpost',
  templateUrl: './savedpost.component.html',
  styleUrls: ['./savedpost.component.css']
})
export class SavedpostComponent implements OnInit {

  posts$:Observable<any>;
  userId:string;
  Like:number;

    numLikes(id:string,Like:number){
        if(isNaN(Like)){
          Like = 1;
        } else{
          Like = Like + 1;
        }
        this.postsService.updateLike(this.userId,id,Like);
      }
    
  delete(id:string){
    this.postsService.delete(this.userId,id);
  } 

  constructor(private postsService:PostsService,
              public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
      this.userId = user.uid;
      console.log(this.userId); 
      this.posts$ = this.postsService.getPost(this.userId);        
                    }
                      ) 
                    }
              }