import { Component, OnInit } from '@angular/core';
import { StudentService } from '../student.service';


@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  displayedColumns: string[] = ['mathematics', 'psychometric', 'pay', 'email','result','delete'];
  students$;
  students=[];  

  delete(id:string){
    this.StudentService.delete(id);
  } 
  constructor(private StudentService:StudentService) { }

  ngOnInit(): void {
        this.students$ = this.StudentService.getStudents();
        this.students$.subscribe(
          docs => {
            this.students = [];
            for (let document of docs){
              const student:any = document.payload.doc.data();
              student.id = document.payload.doc.id;
              this.students.push(student);
            }
          }
        )
      }
  
  }

