import { AuthService } from './../auth.service';
import { Comments } from './../interfaces/comments';
import { PostsService } from './../posts.service';
import { Posts } from './../interfaces/posts';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts$:Observable<Posts>;
  comments$:Observable<Comments>;
  userId;
  postId:number;
  message:string;
  saveState = [];


  add(id:number,title:string,body:string  ){
    this.postsService.add(this.userId,id,title,body); 
    this.message = "Saved for later viewing";
  }


  constructor(private postsService:PostsService,
              public authService:AuthService) { }

  ngOnInit(): void {
    this.posts$ = this.postsService.getPosts();
    this.comments$ = this.postsService.getComments();
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
  })}
}
