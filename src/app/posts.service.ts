import { Posts } from './interfaces/posts';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Comments } from './interfaces/comments';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  urlPosts = "https://jsonplaceholder.typicode.com/posts/";
  urlComments = "https://jsonplaceholder.typicode.com/comments/";

  postsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');


  add(userId:string,id:number,title:string,body:string){
    const posts = {id:id, title:title,body:body};
    this.userCollection.doc(userId).collection('posts').add(posts);
}

  delete(Userid:string, id:string){
    this.db.doc(`users/${Userid}/posts/${id}`).delete(); 
  } 

  updateLike(userId:string,id:string,Like:number){
          this.db.doc(`users/${userId}/posts/${id}`).update(
            {
              Like:Like
            }
          )
        }
    


  getPosts():Observable<Posts>{
    return this.http.get<Posts>(this.urlPosts);
  }

  getComments():Observable<Comments>{
    return this.http.get<Comments>(this.urlComments);
  }

  getPost(userId){
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
     return this.postsCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
                }
             )
            ))
          }
  

  constructor(private http:HttpClient,
              private db:AngularFirestore) { }
}
