import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';


@Injectable({
  providedIn: 'root'
})


export class StudentService {
  studentCollection:AngularFirestoreCollection =  this.db.collection(`students`);;
 
  delete(id:string){
    this.db.doc(`students/${id}`).delete();
  }
  
  addStudent(mathematics:number, psychometric:number, pay:string, result:string, email:string){
    const student = {mathematics:mathematics, psychometric:psychometric, pay:pay, result:result, email:email};
    this.studentCollection.add(student);
  }

  getStudents(){
    this.studentCollection = this.db.collection(`students`);
    return this.studentCollection.snapshotChanges();
  }

  constructor(private db:AngularFirestore) { }
}
